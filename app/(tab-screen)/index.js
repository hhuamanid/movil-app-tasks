import moment from 'moment'
import { useCallback, useState } from 'react'
import { StyleSheet, View, Text, FlatList } from 'react-native'
import { useFocusEffect, useRouter } from 'expo-router'

import TaskStorage from '../../storage/taskStorage'

import TaskItem from '../../components/TaskItem'
import Colors from '../../constants/Colors'

export default function DashboardScreen() {
  const navigation = useRouter()
  const [taskData, setTaskData] = useState([])
  const [numCompletedTask, setNumCompletedTask] = useState(0)
  const [numPendingTask, setNumPendingTask] = useState(0)
  const [numOverdueTask, setNumOverdueTask] = useState(0)
  const [lastCompletedTask, setLastCompletedTask] = useState(null)
  const [lastOverdueTask, setLastOverdueTask] = useState(null)

  useFocusEffect(
    useCallback(() => {
      loadTasksFromStorage()
      return () => {}
    }, [])
  )

  const loadTasksFromStorage = async () => {
    try {
      const currentDate = moment()
      const completedTasks = await TaskStorage.getAllTasks({ completed: true })
      const uncompletedTasks = await TaskStorage.getAllTasks({ completed: false })
      const pendingTasks = uncompletedTasks.filter(({ date }) => currentDate.isSameOrBefore(moment(date)))
      const overdueTasks = uncompletedTasks.filter(({ date }) => currentDate.isAfter(moment(date)))
      const priorityTasks = [...pendingTasks.filter(({ date }) => currentDate.isSameOrBefore(moment(date)))].sort((a, b) => {
        const priorityComparison = parseInt(b.priority) - parseInt(a.priority);
        if (priorityComparison === 0) {
          const dateA = new Date(a.date).getTime();
          const dateB = new Date(b.date).getTime();
          return dateB - dateA;
        }
        return priorityComparison;
      })
      setNumCompletedTask(completedTasks.length)
      setNumPendingTask(pendingTasks.length)
      setNumOverdueTask(overdueTasks.length)
      setLastCompletedTask(completedTasks.at(-1))
      setLastOverdueTask(overdueTasks.at(-1))
      setTaskData(priorityTasks)
    } catch (error) {
      console.error('Error al obtener las tareas desde el storage:', error)
    }
  }

  const handleCompleteTask = async id => {
    const status = await TaskStorage.toggleTaskCompleted(id)
    if (status){
      await loadTasksFromStorage()
    }
  }

  const handleDeleteTask = async id => {
    const status = await TaskStorage.deleteTask(id)
    if (status){
      await loadTasksFromStorage()
    }
  }

  const handleGoToTask = async id => {
    navigation.push(`retrive-task-screen?id=${id}`)
  }

  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.titleText}>Resumen</Text>
      </View>
      <View style={styles.cardContainer}>
        <View style={{ ...styles.card, backgroundColor: Colors.pending }}>
          <Text style={styles.cardNumber}>{numPendingTask}</Text>
          <Text style={styles.cardLabel}>Pendientes</Text>
        </View>
        <View style={{ ...styles.card, backgroundColor: Colors.overdue }}>
          <Text style={styles.cardNumber}>{numOverdueTask}</Text>
          <Text style={styles.cardLabel}>Vencidos</Text>
        </View>
        <View style={{ ...styles.card, backgroundColor: Colors.completed }}>
          <Text style={styles.cardNumber}>{numCompletedTask}</Text>
          <Text style={styles.cardLabel}>Completados</Text>
        </View>
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.titleText}>Última tarea completada</Text>
      </View>
      { lastCompletedTask
        ?
          <View style={{height: 64, marginTop: 20}}>
            <TaskItem 
              title={lastCompletedTask.title} 
              description={lastCompletedTask.description} 
              label="Eliminar"
              onPress={() => handleGoToTask(lastCompletedTask.id)}
              onAction={() => handleDeleteTask(lastCompletedTask.id)}
              borderColor={ Colors.completed }
              backgroundColor={ Colors.completed }
              buttonBackgroundColor={Colors.completedLight}
              textColor="white"
            />
          </View>
        :
          <View style={{height: 64, marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={styles.emptyText}>Sin tareas completadas por el momento.</Text>
          </View>
      }
      <View style={styles.titleContainer}>
        <Text style={styles.titleText}>Última tarea vencida</Text>
      </View>
      { lastOverdueTask
        ?
          <View style={{height: 64, marginTop: 20}}>
            <TaskItem 
              title={lastOverdueTask.title} 
              description={lastOverdueTask.description} 
              label="Completar"
              onPress={() => handleGoToTask(lastOverdueTask.id)}
              onAction={() => handleCompleteTask(lastOverdueTask.id)}
              borderColor={ Colors.overdue }
              backgroundColor={ Colors.overdue }
              buttonBackgroundColor={ Colors.overdueLight }
              textColor="white"
            />
          </View>
        :
          <View style={{height: 64, marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={styles.emptyText}>Sin tareas vencidas por el momento.</Text>
          </View>
      }
      <View style={styles.titleContainer}>
        <Text style={styles.titleText}>Tareas pendientes prioritarias</Text>
      </View>
      {
        taskData.length 
        ? 
          <FlatList
            style={styles.flatList}
            data={taskData}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item, index }) => (
              <>
                <TaskItem 
                  title={item.title} 
                  description={item.description} 
                  label="Completar"
                  onPress={() => handleGoToTask(item.id)}
                  onAction={() => handleCompleteTask(item.id)}
                  buttonBackgroundColor={Colors.pendingLight}
                  borderColor={ Colors.pending }
                  backgroundColor={ Colors.pending }
                  textColor="white"
                />
                {index === taskData.length - 1 && <View style={{ height: 60 }}></View>}
              </>
            )}
            ItemSeparatorComponent={() => <View style={styles.separator}></View>}
          />
        :
          <View style={styles.emptyContainer}>
            <Text style={styles.emptyText}>Sin tareas pendientes por el momento.</Text>
          </View>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  cardContainer: { 
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    marginTop: 20, 
    gap: 8 
  },
  card: {
    flex: 1, 
    paddingHorizontal: 8, 
    paddingVertical: 16, 
    borderRadius: 8, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  cardNumber: { 
    fontSize: 32, 
    fontWeight: 'bold', 
    color: 'white' 
  },
  cardLabel: { 
    fontSize: 12, 
    fontWeight: 'bold', 
    color: 'white', 
    marginTop: 8 
  },
  titleContainer: {
    marginTop: 20,
  },
  titleText: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  flatList: {
    marginTop: 20,
    flex: 1,
  },
  separator: {
    height: 12,
  },
  emptyContainer: {
    marginTop: 20,
    paddingBottom: 60,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    gap: 20,
  },
  emptyIcon: {
    width: 160,
    height: 200,
    backgroundColor: Colors.primaryLight,
    borderRadius: 36,
  },
  emptyText: {
    fontSize: 16,
  },
})
