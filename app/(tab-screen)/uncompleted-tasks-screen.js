import moment from 'moment'
import { useCallback, useEffect, useState } from 'react'
import { StyleSheet, View, Text, FlatList } from 'react-native'
import { useFocusEffect, useRouter } from 'expo-router'

import TaskStorage from '../../storage/taskStorage'

import TaskItem from '../../components/TaskItem'
import Colors from '../../constants/Colors'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function UncompletedTasksScreen() {
  const navigation = useRouter()
  const [taskData, setTaskData] = useState([])
  const [filteredTasks, setFilteredTasks] = useState([])
  const [type, setType] = useState('all')

  useFocusEffect(
    useCallback(() => {
      loadTasksFromStorage()
      return () => {}
    }, [])
  )

  useEffect(() => {
    filterTask(type)
  }, [taskData, type])

  const loadTasksFromStorage = async () => {
    try {
      const taskList = await TaskStorage.getAllTasks({ completed: false })
      const reversedTaskList = [...taskList].reverse()
      setTaskData(reversedTaskList)
    } catch (error) {
      console.error('Error al obtener las tareas desde el storage:', error)
    }
  }

  const filterTask = () => {
    const currentDate = moment()
    const filteredTasks = taskData.filter(({ date }) => (
      (type === 'all') ||
      (type === 'pending' && currentDate.isSameOrBefore(moment(date))) ||
      (type === 'overdue' && currentDate.isAfter(moment(date)))
    ))
    setFilteredTasks(filteredTasks)
  }

  const handleFilterTask = async type => setType(type)

  const handleGoToTask = async id =>  navigation.push(`retrive-task-screen?id=${id}`)

  const handleCompleteTask = async id => {
    const status = await TaskStorage.toggleTaskCompleted(id)
    if (status){
      const taskList = await TaskStorage.getAllTasks({ completed: false })
      setTaskData(taskList)
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.tabContainer}>
        <TouchableOpacity
          style={[styles.tabButton, type === 'all' && styles.activeTab]}
          onPress={() => handleFilterTask('all')}
        >
          <Text style={[styles.tabText, type === 'all' && styles.activeTabText]}>Todos</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.tabButton, type === 'pending' && styles.activeTab]}
          onPress={() => handleFilterTask('pending')}
        >
          <Text style={[styles.tabText, type === 'pending' && styles.activeTabText]}>Pendientes</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.tabButton, type === 'overdue' && styles.activeTab]}
          onPress={() => handleFilterTask('overdue')}
        >
          <Text style={[styles.tabText, type === 'overdue' && styles.activeTabText]}>Vencidos</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.titleContainer}>
        <Text style={styles.titleText}>Lista de tareas pendientes</Text>
      </View>
      {
        filteredTasks.length 
        ? 
          <FlatList
            style={styles.flatList}
            data={filteredTasks}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item, index }) => (
              <>
                <TaskItem 
                  title={item.title} 
                  description={item.description} 
                  label="Completar"
                  onPress={() => handleGoToTask(item.id)}
                  onAction={() => handleCompleteTask(item.id)}
                  buttonBackgroundColor={Colors.primaryLight}
                />
                {index === filteredTasks.length - 1 && <View style={{ height: 60 }}></View>}
              </>
            )}
            ItemSeparatorComponent={() => <View style={styles.separator}></View>}
          />
        :
          <View style={styles.emptyContainer}>
            <View style={styles.emptyIcon}/>
            <Text style={styles.emptyText}>Sin tareas pendientes por el momento.</Text>
          </View>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 20,
  },
  tabButton: {
    paddingVertical: 8,
    paddingHorizontal: 10,
    borderBottomWidth: 2,
    borderColor: 'black',
  },
  tabText: {
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  activeTab: {
    borderColor: Colors.primary,
  },
  activeTabText: {
    color: Colors.primary,
  },
  button: {
    padding: 15,
    borderRadius: 10,
    margin: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  titleContainer: {
    marginTop: 20,
  },
  titleText: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  flatList: {
    marginTop: 20,
    flex: 1,
  },
  separator: {
    height: 12,
  },
  emptyContainer: {
    marginTop: 20,
    paddingBottom: 60,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    gap: 20,
  },
  emptyIcon: {
    width: 160,
    height: 200,
    backgroundColor: Colors.primaryLight,
    borderRadius: 36,
  },
  emptyText: {
    fontSize: 16,
  },
})
