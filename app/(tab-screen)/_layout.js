import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Tabs, useRouter } from 'expo-router';

import Colors from '../../constants/Colors';

const CustomText = ({ isActive, children }) => (
  <Text style={{ 
    fontSize: 11,
    fontWeight: isActive ? 'bold' : 'normal', 
    color: Colors.primary,
  }}>
    { children }
  </Text>
)

const CustomIcon = ({ isActive }) => (
  <View style={{
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: isActive ? Colors.primary : Colors.primaryLight,
  }}/>
)

export default function TabLayout() {
  const navigation = useRouter();

  return (
    <View style={styles.container}>
      <Tabs>
        <Tabs.Screen
          name="index"
          options={{
            title: 'Dashboard',
            tabBarIcon: ({ focused }) => <CustomIcon isActive={ focused }/>,
            tabBarLabel: ({ focused }) => <CustomText isActive={ focused }>Dashboard</CustomText>
          }}
        /> 
        <Tabs.Screen
          name="uncompleted-tasks-screen"
          options={{
            title: 'Tareas pendientes',
            tabBarIcon: ({ focused }) => <CustomIcon isActive={ focused }/>,
            tabBarLabel: ({ focused }) => <CustomText isActive={ focused }>Incompletadas</CustomText>
          }}
        />
        <Tabs.Screen
          name="completed-tasks-screen"
          options={{
            title: 'Tareas completadas',
            tabBarIcon: ({ focused }) => <CustomIcon isActive={ focused }/>,
            tabBarLabel: ({ focused }) => <CustomText isActive={ focused }>Completadas</CustomText>
          }}
        />
        <Tabs.Screen
          name="profile"
          options={{
            title: 'Perfil',
            tabBarIcon: ({ focused }) => <CustomIcon isActive={ focused }/>,
            tabBarLabel: ({ focused }) => <CustomText isActive={ focused }>Perfil</CustomText>
          }}
        />
      </Tabs>

      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 65,
          right: 20,
          backgroundColor: Colors.primary,
          borderRadius: 50,
          width: 60,
          height: 60,
          elevation: 5
        }}
        onPress={() => navigation.push("create-task-screen")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  }
});
