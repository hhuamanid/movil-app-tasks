import React from 'react'
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native'

import Colors from '../../constants/Colors'
import TaskStorage from '../../storage/taskStorage'

export default function ProfileScreen() {
  const resetData = async () => {
    await TaskStorage.resetTasksData()
  }

  const clearData = async () => {
    await TaskStorage.deleteAllTasks()
  }

  return (
    <View style={styles.container}>
      <View style={styles.profileHeader}>
        <View style={styles.avatar}>
          <View style={styles.avatarCircle} />
        </View>
        <Text style={styles.userName}>Nombre de Usuario</Text>
      </View>
      <View style={styles.userInfo}>
        <Text style={styles.label}>Correo Electrónico:</Text>
        <Text style={styles.infoText}>correo@example.com</Text>
        <Text style={styles.label}>Fecha de Nacimiento:</Text>
        <Text style={styles.infoText}>01 de enero de 2000</Text>
        <Text style={styles.label}>Ubicación:</Text>
        <Text style={styles.infoText}>Ciudad, País</Text>
      </View>
      <TouchableOpacity onPress={resetData}>
        <Text style={styles.resetButton}>Datos de Prueba</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={clearData}>
        <Text style={styles.resetButton}>Eliminar Datos</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  profileHeader: {
    alignItems: 'center',
    marginBottom: 40,
  },
  avatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    backgroundColor: Colors.primaryLight,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarCircle: {
    width: 90,
    height: 90,
    borderRadius: 45,
    backgroundColor: Colors.primary,
  },
  userName: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 10,
  },
  userInfo: {
    marginBottom: 20,
  },
  label: {
    fontSize: 14,
    fontWeight: 'bold',
    marginBottom: 4
  },
  infoText: {
    fontSize: 14,
    marginTop: 5,
    marginBottom: 20
  },
  resetButton: {
    fontSize: 12,
    fontWeight: 'bold',
    color: Colors.primary,
    marginBottom: 20,
  }
})
