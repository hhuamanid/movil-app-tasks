import { useCallback, useState } from 'react'
import { StyleSheet, View, Text, FlatList } from 'react-native'
import { useFocusEffect, useRouter } from 'expo-router'

import TaskStorage from '../../storage/taskStorage'

import TaskItem from '../../components/TaskItem'
import Colors from '../../constants/Colors'

export default function CompletedTasksScreen() {
  const navigation = useRouter()
  const [taskData, setTaskData] = useState([])

  useFocusEffect(
    useCallback(() => {
      loadTasksFromStorage()
      return () => {}
    }, [])
  )

  const loadTasksFromStorage = async () => {
    try {
      const taskList = await TaskStorage.getAllTasks({ completed: true })
      const reversedTaskList = taskList.reverse()
      setTaskData(reversedTaskList)
    } catch (error) {
      console.error('Error al obtener las tareas desde el storage:', error)
    }
  }

  const handleDeleteTask = async id => {
    const status = await TaskStorage.deleteTask(id)
    if (status){
      const taskList = await TaskStorage.getAllTasks({ completed: true })
      setTaskData(taskList)
    }
  }

  const handleGoToTask = async id => {
    navigation.push(`retrive-task-screen?id=${id}`)
  }

  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.titleText}>Lista de tareas completadas</Text>
      </View>
      {
        taskData.length 
        ? 
          <FlatList
            style={styles.flatList}
            data={taskData}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item, index }) => (
              <>
                <TaskItem 
                  title={item.title} 
                  description={item.description} 
                  label="Eliminar"
                  onPress={() => handleGoToTask(item.id)}
                  onAction={() => handleDeleteTask(item.id)}
                  buttonBackgroundColor={Colors.primaryLight}
                />
                {index === taskData.length - 1 && <View style={{ height: 60 }}></View>}
              </>
            )}
            ItemSeparatorComponent={() => <View style={styles.separator}></View>}
          />
        :
          <View style={styles.emptyContainer}>
            <View style={styles.emptyIcon}/>
            <Text style={styles.emptyText}>Sin tareas completadas por el momento.</Text>
          </View>
      }
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
  },
  titleContainer: {
    marginTop: 20,
  },
  titleText: {
    fontSize: 14,
    fontWeight: 'bold',
  },
  flatList: {
    marginTop: 20,
    flex: 1,
  },
  separator: {
    height: 12,
  },
  emptyContainer: {
    marginTop: 20,
    paddingBottom: 60,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    gap: 20,
  },
  emptyIcon: {
    width: 160,
    height: 200,
    backgroundColor: Colors.primaryLight,
    borderRadius: 36,
  },
  emptyText: {
    fontSize: 16,
  },
})
