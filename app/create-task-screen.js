import moment from 'moment'
import React, { useState } from 'react'
import { useRouter } from 'expo-router'
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'

import TaskStorage from '../storage/taskStorage'

import DateTimeInput from '../components/DateTimeInput'
import Colors from '../constants/Colors'

export default function CreateTaskScreen () {
  const navigation = useRouter()

  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [date, setDate] = useState(moment().format("YYYY-MM-DD HH:mm:ss"))
  const [priority, setPriority] = useState('')

  const handleSubmit = async() => {
    
    await TaskStorage.createTask({ title, description, date, priority })
    navigation.back()
  }

  return (
    <View style={styles.container}>
      <Text style={styles.label}>Título:</Text>
      <TextInput
        style={styles.input}
        onChangeText={setTitle}
        value={title}
        placeholder="Ingrese el título de la tarea"
      />

      <Text style={styles.label}>Descripción:</Text>
      <TextInput
        style={styles.input}
        onChangeText={setDescription}
        value={description}
        placeholder="Ingrese la descripción de la tarea"
        multiline={true}
      />

      <Text style={styles.label}>Fecha de Vencimiento:</Text>
      <DateTimeInput 
        datetime={ date }
        onDatetimeChange={ value => setDate(moment(value).format("YYYY-MM-DD HH:mm:ss"))}
      />

      <Text style={styles.label}>Prioridad (1-10):</Text>
      <TextInput
        style={styles.input}
        onChangeText={(text) => {
          const num = parseInt(text)
          if (!isNaN(num) && num >= 1 && num <= 10)
            setPriority(num.toString())
          else 
            setPriority('')
        }}
        value={priority}
        keyboardType="numeric"
        placeholder="Ingrese la prioridad (1-10)"
      />

      <TouchableOpacity style={styles.button} title="Registrar Tarea" onPress={handleSubmit}>
        <Text style={styles.buttonLabel}>Registrar Tarea</Text>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  label: {
    fontSize: 14, 
    fontWeight: 'bold',
    marginBottom: 10
  },
  input: {
    height: 40,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: Colors.primary,
    padding: 10,
    borderRadius: 10,
  },
  buttonLabel: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16, 
  },
})
