import React, { useCallback, useState } from 'react';
import { useFocusEffect, useLocalSearchParams } from 'expo-router';
import { StyleSheet, View, Text } from 'react-native';

import TaskStorage from '../storage/taskStorage'

export default function RetriveTaskScreen() {
  const { id } = useLocalSearchParams()
  const [ task, setTask ] = useState(null)
  TaskStorage.retrieveTask(id)

  useFocusEffect(
    useCallback(() => {
      loadTaskFromStorage()
      return () => {}
    }, [])
  )

  const loadTaskFromStorage = async () => {
    try {
      const task = await TaskStorage.retrieveTask(id)
      setTask(task)
    } catch (error) {
      console.error('Error al obtener la tarea desde el storage:', error)
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.fieldContainer}>
        <Text style={styles.fieldLabel}>Título:</Text>
        <Text style={styles.fieldValue}>{task?.title}</Text>
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.fieldLabel}>Descripción:</Text>
        <Text style={styles.fieldValue}>{task?.description}</Text>
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.fieldLabel}>Prioridad:</Text>
        <Text style={styles.fieldValue}>{task?.priority}</Text>
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.fieldLabel}>Fecha de Vencimiento:</Text>
        <Text style={styles.fieldValue}>{task?.date}</Text>
      </View>
      <View style={styles.fieldContainer}>
        <Text style={styles.fieldLabel}>Estado:</Text>
        <Text style={styles.fieldValue}>{task?.completed ? 'Completado' : 'No Completado'}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    paddingTop: 20,
  },
  fieldContainer: {
    marginBottom: 16,
  },
  fieldLabel: {
    fontSize: 14, 
    fontWeight: 'bold',
    marginBottom: 10
  },
  fieldValue: {
    marginBottom: 12,
  },
});
