import { Stack } from 'expo-router';

const WelcomeScreen = () => {
  return (
    <Stack>
      <Stack.Screen 
        name="(tab-screen)" 
        options={{ 
          headerShown: false 
        }} 
      />
      <Stack.Screen 
        name="create-task-screen" 
        options={{ 
          title: "Crear nueva tarea",
          presentation: 'modal' 
        }}
      />
      <Stack.Screen 
        name="retrive-task-screen" 
        options={{ 
          title: "Detalle de tarea",
          presentation: 'modal' 
        }}
      />
    </Stack>
  );
};


export default WelcomeScreen;