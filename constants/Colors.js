export default {
  primary: '#0284c7',
  primaryLight: '#e0f2fe',

  completed: '#10b981',
  completedLight: '#d1fae5',
  overdue: '#ef4444',
  overdueLight: '#fee2e2', 
  pending: '#06b6d4',
  pendingLight: '#cffafe'
};
