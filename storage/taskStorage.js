import AsyncStorage from '@react-native-async-storage/async-storage'
import uuid from 'react-native-uuid'
import data from './data.json'

export default class TaskService {
  static async createTask(task) {
    try {
      const tasks = await this.getAllTasks()
      const newTask = { id: uuid.v4(), completed: false, ...task }
      const updatedTasks = [...tasks, newTask]
      await AsyncStorage.setItem('tasks', JSON.stringify(updatedTasks))
      return true
    } catch (error) {
      console.error('Error al crear la tarea:', error)
      return false
    }
  }

  static async retrieveTask(id) {
    try {
      const tasks = await this.getAllTasks()
      const task = tasks.find((task) => task.id === id)
      return task || null
    } catch (error) {
      console.error('Error al recuperar la tarea por ID:', error)
      return null
    }
  }

  static async getAllTasks({ completed = null } = {}) {
    try {
      const tasksJson = await AsyncStorage.getItem('tasks')
      const tasks = tasksJson ? JSON.parse(tasksJson) : []
      if (completed === null) {
        return tasks
      } else {
        return tasks.filter((task) => task.completed === completed)
      }
    } catch (error) {
      console.error('Error al obtener las tareas:', error)
      return []
    }
  }

  static async updateTask(taskId, updatedTask) {
    try {
      const tasks = await this.getAllTasks()
      const updatedTasks = tasks.map((task) =>
        task.id === taskId ? { ...task, ...updatedTask } : task
      )
      await AsyncStorage.setItem('tasks', JSON.stringify(updatedTasks))
      return true
    } catch (error) {
      console.error('Error al actualizar la tarea:', error)
      return false
    }
  }

  static async deleteTask(taskId) {
    try {
      const tasks = await this.getAllTasks()
      const updatedTasks = tasks.filter((task) => task.id !== taskId)
      await AsyncStorage.setItem('tasks', JSON.stringify(updatedTasks))
      return true
    } catch (error) {
      console.error('Error al eliminar la tarea:', error)
      return false
    }
  }

  static async toggleTaskCompleted(taskId) {
    try {
      const tasks = await this.getAllTasks()
      const updatedTasks = tasks.map((task) =>
        task.id === taskId ? { ...task, completed: !task.completed } : task
      )
      await AsyncStorage.setItem('tasks', JSON.stringify(updatedTasks))
      return true
    } catch (error) {
      console.error('Error al cambiar el estado de la tarea:', error)
      return false
    }
  }

  static async deleteAllTasks() {
    try {
      await AsyncStorage.removeItem('tasks')
      return true
    } catch (error) {
      console.error('Error al eliminar todas las tareas:', error)
      return false
    }
  }

  static async resetTasksData() {
    await AsyncStorage.removeItem('tasks')
    await AsyncStorage.setItem('tasks', JSON.stringify(data))
  }
}
