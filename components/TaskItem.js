import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Colors from "../constants/Colors";

export default function TaskItem({ title, description, label, onPress, onAction, borderColor = Colors.primary, backgroundColor = "white", textColor = "black", buttonBackgroundColor = "white" }) {
  const maxLenTitle = 24;
  const maxLenDescription = 32;
  const truncatedTitle = title.length > maxLenTitle ? title.substring(0, maxLenTitle) + "..." : title;
  const truncatedDescription = description.length > maxLenDescription ? description.substring(0, maxLenDescription) + "..." : description;

  return (
    <TouchableOpacity style={[styles.container, { borderColor: borderColor, backgroundColor: backgroundColor }]} onPress={onPress}>
      <View style={styles.taskInfo}>
        <Text style={[styles.title, { color: textColor }]}>{truncatedTitle}</Text>
        <Text style={[styles.description, { color: textColor }]}>{truncatedDescription}</Text>
      </View>
      <TouchableOpacity onPress={onAction} style={[styles.deleteButton, { backgroundColor: buttonBackgroundColor }]}>
        <Text style={styles.deleteButtonText}>{label}</Text>
      </TouchableOpacity>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 64,
    paddingVertical: 12,
    paddingHorizontal: 16,
    borderWidth: 2,
    borderRadius: 16,
  },
  taskInfo: {
    flex: 1,
  },
  title: {
    fontSize: 14,
    fontWeight: "bold",
    marginBottom: 5,
  },
  description: {
    fontSize: 12,
  },
  deleteButton: {
    width: 96,
    height: 40,
    paddingHorizontal: 10,
    paddingVertical: 5,
    borderRadius: 5,
    alignItems: "center",
    justifyContent: 'center',
  },
  deleteButtonText: {
    fontWeight: "bold",
  },
});
