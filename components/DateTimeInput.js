import moment from 'moment'
import React, { useState } from 'react'
import { View, TouchableOpacity, TextInput, StyleSheet } from 'react-native'
import DateTimePicker from '@react-native-community/datetimepicker'

import Colors from '../constants/Colors'

export default DateTimeInput = (props) => {
  const [datetime, setDatetime] = useState(props.datetime ? new Date(props.datetime) : new Date())
  const [mode, setMode] = useState('date')
  const [show, setShow] = useState(false)

  const onChange = (event, selectedValue) => {
    setShow(false)
    if (mode == 'date') {
      if (event.type === 'set') {
        const stringDate = moment(selectedValue).format("YYYY-MM-DD")
        const stringTime = moment(datetime).format("HH:mm:ss")
        setDatetime(new Date(`${stringDate} ${stringTime}`))
        props.onDatetimeChange(new Date(`${stringDate} ${stringTime}`))
      }
      setMode('time')
      setShow(true)
    } else {
      if (event.type === 'set') {
        const stringDate = moment(datetime).format("YYYY-MM-DD")
        const stringTime = moment(selectedValue).format("HH:mm:ss")
        setDatetime(new Date(`${stringDate} ${stringTime}`))
        props.onDatetimeChange(new Date(`${stringDate} ${stringTime}`))
      }
      setShow(false)
      setMode('date')
    }
  }

  const showDatePicker = () => setShow(true)

  return (
    <View>
      <TouchableOpacity onPress={ showDatePicker }>
        <TextInput
          style={styles.input}
          placeholder="Selecciona la fecha y hora"
          value={`${moment(datetime).format("YYYY-MM-DD HH:mm:ss")}`}
          editable={false}
        />
      </TouchableOpacity>
      {show && (
        <DateTimePicker
          is24Hour
          display="default"
          value={datetime}
          mode={mode}
          onChange={onChange}
        />
      )}
    </View>
  )
}
const styles = StyleSheet.create({
  input: {
    height: 40,
    borderColor: Colors.primary,
    borderWidth: 1,
    borderRadius: 10,
    marginBottom: 20,
    paddingHorizontal: 10,
    color: "black"
  },
})
